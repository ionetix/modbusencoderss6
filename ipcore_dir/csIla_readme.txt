The following files were generated for 'csIla' in directory
/home/ise/git/master/ModbusEncodersS6/ipcore_dir/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * csIla.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * csIla.cdc
   * csIla.constraints/csIla.ucf
   * csIla.constraints/csIla.xdc
   * csIla.ncf
   * csIla.ngc
   * csIla.ucf
   * csIla.v
   * csIla.veo
   * csIla.xdc
   * csIla_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * csIla.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * csIla.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * csIla.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * csIla.gise
   * csIla.xise

Deliver Readme:
   Readme file for the IP.

   * csIla_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * csIla_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

