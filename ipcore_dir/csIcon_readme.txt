The following files were generated for 'csIcon' in directory
/home/ise/git/master/ModbusEncodersS6/ipcore_dir/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * csIcon.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * csIcon.constraints/csIcon.ucf
   * csIcon.constraints/csIcon.xdc
   * csIcon.ngc
   * csIcon.ucf
   * csIcon.v
   * csIcon.veo
   * csIcon.xdc
   * csIcon_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * csIcon.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * csIcon.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * csIcon.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * csIcon.gise
   * csIcon.xise

Deliver Readme:
   Readme file for the IP.

   * csIcon_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * csIcon_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

