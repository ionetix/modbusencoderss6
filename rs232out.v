`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:34:36 01/28/2020 
// Design Name: 
// Module Name:    rs232out 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rs232out(
	input clk,
	input [12:0] baud,
	input [7:0] char,
	input start,
	output TX,
	output busy
	);

	reg [8:0] shifter = 9'h1FF;
	reg [3:0] state = 0;
	reg [12:0] delay = 0;
	wire next;
	assign busy = |state;
	assign TX = shifter[0];
	assign next = delay >= baud;
	
	always @ (posedge clk)
	begin
		// load shifter with char and start bit
		if (~|state & start)
		begin
			delay <= 0;
			shifter <= {char, 1'b0};
			state <= state + 1'b1;
		end
		// shift out start bit, char, and stop bit
		else if (|state & next)
		begin
			delay <= 0;
			shifter <= {1'b1, shifter[8:1]};
			state <= (state >= 4'hA) ? 4'h0 : state + 1'b1;
		end
		else
			delay <= delay + 1'b1;
	end
endmodule
