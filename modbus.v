`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:09:28 01/30/2020 
// Design Name: 
// Module Name:    modbus 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module modbus(
	input clk,
	input [12:0] baud,
	input [7:0] rxChar,
	input rxCharValid,
	output reg [7:0] txChar = 8'h00,
	output reg txCharValid = 1'b0,
	input txBusy,
	input [15:0] reg0,
	input [15:0] reg1,
	input [15:0] reg2,
	input [15:0] reg3,
	input [15:0] reg4,
	input [15:0] reg5,
	input [15:0] reg6,
	input [15:0] reg7,
	input [15:0] reg8,
	input [15:0] reg9,
	output readRegs
	);
	
	reg [19:0] timeoutCtr = 20'h000000;
	reg timeout = 1'b0;
	reg [3:0] rxCharCtr = 4'h0;
	reg [7:0] slaveAddr = 8'h00;
	reg cmdType = 1'b0;
	reg [15:0] cmdAddr = 16'h0000;
	reg [15:0] cmdLen = 16'h0000;
	reg [15:0] cmdCRC = 16'h0000;
	reg [2:0] txState = 3'h0;
	reg [15:0] dataPtr = 16'h0000;
	reg [15:0] dataEnd = 16'h0000;
	
	wire [15:0] crcIn;
	wire [15:0] crcOut;
	
	assign readRegs = rxCharCtr[3] & (cmdCRC == crcIn) & cmdType;

	always @ (posedge clk)
	begin
		dataEnd <= cmdAddr + cmdLen[7:0];
		if (rxCharValid & ~txBusy)
		begin
			timeoutCtr <= 23'h000000;
			rxCharCtr <= rxCharCtr + 1'b1;
			case (rxCharCtr)
				4'h0: slaveAddr <= rxChar;
				4'h1: cmdType <= rxChar == 8'h04;
				4'h2: cmdAddr[15:8] <= rxChar;
				4'h3: cmdAddr[7:0] <= rxChar;
				4'h4: cmdLen[15:8] <= rxChar;
				4'h5: cmdLen[7:0] <= rxChar;
				4'h6: cmdCRC[7:0] <= rxChar;
				4'h7: cmdCRC[15:8] <= rxChar;
			endcase
		end
		else
		begin
			if (timeoutCtr > {baud, 7'h00})
			begin
				timeoutCtr <= 20'h00000;
				timeout <= |rxCharCtr;
				rxCharCtr <= 4'h0;
			end
			else
			begin
				timeout <= 0;
				timeoutCtr <= timeoutCtr + 1'b1;
			end
		end
		if (rxCharCtr[3])
		begin
			rxCharCtr <= 4'h0;
			cmdLen[7:0] <= |cmdLen[15:7] ? 8'hFE : {cmdLen[6:0], 1'b0};
		end

		// modbus response logic
		case (txState)
			0: // wait for valid modbus command
			begin
				dataPtr <= cmdAddr;
				txChar <= 8'h00;
				txCharValid <= 1'b0;
				if (readRegs & ~txBusy)
					txState <= txState + 1'b1;
			end
			1: // repeat slave address from command
			begin
				txChar <= slaveAddr;
				txCharValid <= ~txBusy & ~txCharValid;
				if (~txBusy & ~txCharValid)
					txState <= txState + 1'b1;
			end
			2: // repeat function code from command
			begin
				txChar <= 8'h04;
				txCharValid <= ~txBusy & ~txCharValid;
				if (~txBusy & ~txCharValid)
					txState <= txState + 1'b1;
			end
			3: // send number of data bytes
			begin
				txChar <= cmdLen[7:0];
				txCharValid <= ~txBusy & ~txCharValid;
				if (~txBusy & ~txCharValid)
					txState <= txState + 1'b1;
			end
			4: // send data bytes (dataPtr increments once before first byte is sent)
			begin
				case (dataPtr)
					1: txChar <= reg0[15:8];
					2: txChar <= reg0[7:0];
					3: txChar <= reg1[15:8];
					4: txChar <= reg1[7:0];
					5: txChar <= reg2[15:8];
					6: txChar <= reg2[7:0];
					7: txChar <= reg3[15:8];
					8: txChar <= reg3[7:0];
					9: txChar <= reg4[15:8];
					10: txChar <= reg4[7:0];
					11: txChar <= reg5[15:8];
					12: txChar <= reg5[7:0];
					13: txChar <= reg6[15:8];
					14: txChar <= reg6[7:0];
					15: txChar <= reg7[15:8];
					16: txChar <= reg7[7:0];
					17: txChar <= reg8[15:8];
					18: txChar <= reg8[7:0];
					19: txChar <= reg9[15:8];
					20: txChar <= reg9[7:0];
					default: txChar <= 8'h00;
				endcase
				txCharValid <= ~txBusy & ~txCharValid;
				if (txCharValid)
					dataPtr <= dataPtr + 1'b1;
				if (~txBusy & (dataPtr == dataEnd) & ~txCharValid)
					txState <= txState + 1'b1;
			end
			5: // send CRC upper byte
			begin
				txChar <= crcOut[7:0];
				txCharValid <= ~txBusy & ~txCharValid;
				if (~txBusy & ~txCharValid)
					txState <= txState + 1'b1;
			end
			6: // send CRC lower byte
			begin
				txChar <= crcOut[15:8];
				txCharValid <= ~txBusy & ~txCharValid;
				if (~txBusy & ~txCharValid)
					txState <= txState + 1'b1;
			end
			7: // wait for last byte to finish transmitting
			begin
				txChar <= 8'h00;
				txCharValid <= 0;
				if (~txBusy)
					txState <= txState + 1'b1;
			end
		endcase
	end

	crc crcRX(.clk(clk), .reset(timeout | rxCharCtr[3]), .word(rxChar), .wordValid(rxCharValid & (rxCharCtr < 6)), .crc(crcIn));
	crc crcTX(.clk(clk), .reset(rxCharCtr[3]), .word(txChar), .wordValid(txCharValid & (txState < 6)), .crc(crcOut));
endmodule
