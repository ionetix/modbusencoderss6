`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:28:39 01/27/2020 
// Design Name: 
// Module Name:    encoderTop 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module encoderTop(
	input GCLK,
	input [9:0] A,
	input [9:0] B,
	input RESET,
	input RX,
	output TX,
	output reg LED_EN = 0,
	output reg LED_RX = 0,
	output reg LED_TX = 0,
	output reg [3:0] LED = 4'h1,
	input [2:0] DIP
	);

	wire clk8;
	BUFG BUFG_clk8 (.O(clk8), .I(GCLK));
	
	reg [12:0] baud = 0;					// baud rate setting in clk8 counts per bit
	wire [15:0] count0;					// encoder 0 count
	wire [15:0] count1;					// encoder 1 count
	wire [15:0] count2;					// encoder 2 count
	wire [15:0] count3;					// encoder 3 count
	wire [15:0] count4;					// encoder 4 count
	wire [15:0] count5;					// encoder 5 count
	wire [15:0] count6;					// encoder 6 count
	wire [15:0] count7;					// encoder 7 count
	wire [15:0] count8;					// encoder 8 count
	wire [15:0] count9;					// encoder 9 count
	reg [15:0] latched0 = 16'h0000;	// latched encoder 0 count
	reg [15:0] latched1 = 16'h0000;	// latched encoder 1 count
	reg [15:0] latched2 = 16'h0000;	// latched encoder 2 count
	reg [15:0] latched3 = 16'h0000;	// latched encoder 3 count
	reg [15:0] latched4 = 16'h0000;	// latched encoder 4 count
	reg [15:0] latched5 = 16'h0000;	// latched encoder 5 count
	reg [15:0] latched6 = 16'h0000;	// latched encoder 6 count
	reg [15:0] latched7 = 16'h0000;	// latched encoder 7 count
	reg [15:0] latched8 = 16'h0000;	// latched encoder 8 count
	reg [15:0] latched9 = 16'h0000;	// latched encoder 9 count
	wire [7:0] charIn;					// character in from Modbus
	wire validIn;							// character valid from Modbus
	wire [7:0] charOut;
	wire validOut;
	wire busyTX;
	reg [20:0] ledRXDly = 0;			// timer to keep LED on after RX
	reg [20:0] ledTXDly = 0;			// timer to keep LED on after TX
	reg [20:0] ledDly = 0;				// timer for status LED flashing
	wire latchRegs;
	
	always @ (posedge clk8)
	begin
		// All 8 characters have been received
		// Process the command if it is valid
		if (latchRegs)
		begin
			latched0 <= count0;
			latched1 <= count1;
			latched2 <= count2;
			latched3 <= count3;
			latched4 <= count4;
			latched5 <= count5;
			latched6 <= count6;
			latched7 <= count7;
			latched8 <= count8;
			latched9 <= count9;
		end
		
		// LED timers
		ledDly <= ledDly + 1'b1;
		if (&ledDly)
			LED <= {LED[2:0], LED[3]};
		if (~RX)
			ledRXDly <= 21'h1FFFFF;
		else if (|ledRXDly)
			ledRXDly <= ledRXDly - 1'b1;
		LED_RX <= |ledRXDly;
		if (~TX)
			ledTXDly <= 21'h1FFFFF;
		else if (|ledTXDly)
			ledTXDly <= ledTXDly - 1'b1;
		LED_TX <= |ledTXDly;
		LED_EN <= ~RESET;
	
		// read DIP switches to set baud rate
		case (DIP)
			3'h0: baud <= 13'd69;		// 115200 baud
			3'h1: baud <= 13'd139;		// 57600 baud
			3'h2: baud <= 13'd208;		// 38400 baud
			3'h3: baud <= 13'd417;		// 19200 baud
			3'h4: baud <= 13'd833;		// 9600 baud
			3'h5: baud <= 13'd1667;		// 4800 baud
			3'h6: baud <= 13'd3333;		// 2400 baud
			3'h7: baud <= 13'd6667;		// 1200 baud
		endcase
	end

	encoder decode0(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[0]), .b(B[0]), .count(count0));
	encoder decode1(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[1]), .b(B[1]), .count(count1));
	encoder decode2(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[2]), .b(B[2]), .count(count2));
	encoder decode3(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[3]), .b(B[3]), .count(count3));
	encoder decode4(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[4]), .b(B[4]), .count(count4));
	encoder decode5(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[5]), .b(B[5]), .count(count5));
	encoder decode6(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[6]), .b(B[6]), .count(count6));
	encoder decode7(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[7]), .b(B[7]), .count(count7));
	encoder decode8(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[8]), .b(B[8]), .count(count8));
	encoder decode9(.clk(clk8), .reset(RESET), .read(latchRegs), .a(A[9]), .b(B[9]), .count(count9));
	rs232in cmdIn(.clk(clk8), .baud(baud), .RX(RX), .char(charIn), .valid(validIn));
	modbus inputRegs(.clk(clk8), .baud(baud), .rxChar(charIn), .rxCharValid(validIn), .txChar(charOut),
		.txCharValid(validOut), .txBusy(busyTX), .reg0(latched0), .reg1(latched1), .reg2(latched2),
		.reg3(latched3), .reg4(latched4), .reg5(latched5), .reg6(latched6), .reg7(latched7),
		.reg8(latched8), .reg9(latched9), .readRegs(latchRegs));
	rs232out dataOut(.clk(clk8), .baud(baud), .char(charOut), .start(validOut), .TX(TX), .busy(busyTX));
endmodule
