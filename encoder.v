`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:30:27 01/27/2020 
// Design Name: 
// Module Name:    encoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module encoder(
    input clk,
    input reset,
	 input read,
    input a,
    input b,
    output reg [15:0] count = 16'h0000
    );

	reg [1:0] encoderNow = 0;
	reg [1:0] encoderLast = 0;
	
	always @ (posedge clk)
	begin
		encoderNow <= {a, b};
		encoderLast <= encoderNow;
		if (reset)
			count <= 16'h0000;
		else
		begin
			if (^encoderNow == ^encoderLast)
				count <= read ? 16'h0000 : count;
			else if (encoderNow[1] == encoderLast[0])
				count <= read ? 16'hFFFF : count - 1'b1;
			else
				count <= read ? 16'h0001 : count + 1'b1;
		end
	end
endmodule
