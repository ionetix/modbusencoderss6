`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:14:15 01/28/2020 
// Design Name: 
// Module Name:    rs232in 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rs232in(
	input clk,
	input [12:0] baud,
	input RX,
	output reg [7:0] char = 8'h00,
	output reg valid = 0
	);

	reg [7:0] shifter = 0;
	reg [3:0] state = 0;
	reg [12:0] delay = 0;
	wire cycle;
	wire halfCycle;
	
	assign cycle = delay >= baud;
	assign halfCycle = delay >= {1'b0, baud[12:1]};
	
	always @ (posedge clk)
	begin
		valid <= (state == 4'hA) & cycle;
		if (cycle)
		begin
			shifter <= {RX, shifter[7:1]};
			if (state == 4'hA)
				char <= shifter;
		end
		if ((~|state & ~RX) | ((state == 4'h1) & halfCycle) | (|state[3:1] & cycle))
		begin
			state <= (state >= 4'hA) ? 4'h0 : state + 1'b1;
			delay <= 0;
		end
		else
			delay <= delay + 1'b1;
	end
endmodule
